# request l'ho messo io #
from flask import abort, flash, redirect, render_template, url_for, request
from flask_login import current_user, login_required
from flask_wtf import FlaskForm
from . import admin
from .forms import WordForm
from .. import db
from ..models import Word
from .. import dictionariesUploadSet

###
# Roba mia
from wtforms import FileField
#
###
# Roba mia
class MyForm(FlaskForm):
    dictionary = FileField('dictionary')
#
def check_admin():
    # prevent non-admins from accessing the page
    if not current_user.is_admin:
        abort(403)


@admin.route('/words', methods=['GET', 'POST'])
@login_required
def list_words():
    """
    List all words
    """
    ###
    # Roba mia
    form = MyForm()
    #
    check_admin()

    words = Word.query.all()

    ###
    # Roba mia

    if form.validate_on_submit():
        #filename = dictionariesUploadSet.save(form.dictionary.data)
        filename = dictionariesUploadSet.save(form.dictionary.data)
        print("uploads/dictionaries/"+filename)
        file = open("uploads/dictionaries/"+filename, "r")
        type(file)
        for line in file:
            if line.__contains__("["):
                # deve essere line=line.replace, mettere solo line.replace non funziona
                line = line.replace("[", "")
            if line.__contains__("]"):
                # deve essere line=line.replace, mettere solo line.replace non funziona
                line = line.replace("]", "")
            if line.__contains__("a\'"):
                # deve essere line=line.replace, mettere solo line.replace non funziona
                line = line.replace("a\'", "à")
            if line.__contains__("e\'"):
                # deve essere line=line.replace, mettere solo line.replace non funziona
                line = line.replace("e\'", "è")
            if line.__contains__("i\'"):
                # deve essere line=line.replace, mettere solo line.replace non funziona
                line = line.replace("i\'", "ì")
            if line.__contains__("o\'"):
                # deve essere line=line.replace, mettere solo line.replace non funziona
                line = line.replace("o\'", "ò")
            if line.__contains__("u\'"):
                # deve essere line=line.replace, mettere solo line.replace non funziona
                line = line.replace("u\'", "ù")
            if line.__contains__("d\'"):
                # deve essere line=line.replace, mettere solo line.replace non funziona
                line = line.replace("d\'", "d'")
            word = Word(name=line, description=line)
            db.session.add(word)
            db.session.commit()
        file.close()
        return f'Filename: {filename}'
        #return render_template('admin/words/words.html', words=words, title="Words", form=form)

    # form=form dentro al return è stato messo da me

    return render_template('admin/words/words.html',
                           words=words, title="Words", form=form)



@admin.route('/words/add', methods=['GET', 'POST'])
@login_required
def add_word():
    """
    Add a department to the database
    """
    check_admin()
    # file = open("testo.txt", "r")
    # file_write = open("testo_write.txt", "w")
    add_word = True

    form = WordForm()
    if form.validate_on_submit():
        # for line in file:
        #     if line.__contains__("a\'"):
        #         # deve essere line=line.replace, mettere solo line.replace non funziona
        #         line = line.replace("a\'", "à")
        #     if line.__contains__("e\'"):
        #         # deve essere line=line.replace, mettere solo line.replace non funziona
        #         line = line.replace("e\'", "è")
        #     if line.__contains__("i\'"):
        #         # deve essere line=line.replace, mettere solo line.replace non funziona
        #         line = line.replace("i\'", "ì")
        #     if line.__contains__("o\'"):
        #         # deve essere line=line.replace, mettere solo line.replace non funziona
        #         line = line.replace("o\'", "ò")
        #     if line.__contains__("u\'"):
        #         # deve essere line=line.replace, mettere solo line.replace non funziona
        #         line = line.replace("u\'", "ù")
        #     file_write.write(line)
        #     word = Word(name=line, description=line)
        #     db.session.add(word)
        #     db.session.commit()
        word = Word(name=form.name.data, description=form.description.data)
        try:
            # add word to the database
            db.session.add(word)
            db.session.commit()
            flash('You have successfully added a new word.')
        except:
            # in case department name already exists
            flash('Error: word name already exists.')

        # redirect to words page
        return redirect(url_for('admin.list_words'))



    # load department template
    return render_template('admin/words/word.html', action="Add",
                           add_word=add_word, form=form,
                           title="Add Word")




@admin.route('/words/edit/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_word(id):
    """
    Edit a word
    """
    check_admin()

    add_word = False

    word = Word.query.get_or_404(id)
    form = WordForm(obj=word)
    if form.validate_on_submit():
        word.name = form.name.data
        word.description = form.description.data
        db.session.commit()
        flash('You have successfully edited the word.')

        # redirect to the words page
        return redirect(url_for('admin.list_words'))

    form.description.data = word.description
    form.name.data = word.name
    return render_template('admin/words/word.html', action="Edit",
                           add_word=add_word, form=form,
                           word=word, title="Edit Word")



@admin.route('/words/delete/<int:id>', methods=['GET', 'POST'])
@login_required
def delete_word(id):
    """
    Delete a word from the database
    """
    check_admin()

    word = Word.query.get_or_404(id)
    db.session.delete(word)
    db.session.commit()
    flash('You have successfully deleted the word.')

    # redirect to the words page
    return redirect(url_for('admin.list_words'))

    return render_template(title="Delete Word")


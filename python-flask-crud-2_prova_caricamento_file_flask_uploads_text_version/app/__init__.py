# third-party imports
from flask import Flask
from flask_bootstrap import Bootstrap
from flask_login import LoginManager
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
###
# Roba mia
from flask_uploads import configure_uploads, IMAGES, UploadSet
##
# local imports
from config import app_config

db = SQLAlchemy()
login_manager = LoginManager()
###
# Roba mia
# clonato più sotto in create_app
#images = UploadSet('images', IMAGES)
##

def create_app(config_name):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(app_config[config_name])
    app.config.from_pyfile('config.py')
    ###
    # Roba mia
    app.config['SECRET_KEY'] = 'thisisasecret'
    #app.config['UPLOAD_IMAGES_DEST'] = 'uploads/images'
    # Ho dovuto mettere UPLOADS_DEFAULT_DEST anziché UPLOAD_IMAGES_DEST
    # perché il programma diceva che non avevo impostato una destinazione
    # per il set, e pare che UPLOAD_IMAGES_DEST impostando una destinazione di
    # default per tutti i set di upload, prevenga questo problema
    app.config['UPLOADS_DEFAULT_DEST'] = 'uploads/images'
    images = UploadSet('images', IMAGES)
    configure_uploads(app, images)
    ###

    Bootstrap(app)
    db.init_app(app)
    login_manager.init_app(app)
    login_manager.login_message = "You must be logged in to access this page."
    login_manager.login_view = "auth.login"
    migrate = Migrate(app, db)

    from app import models

    from .admin import admin as admin_blueprint
    app.register_blueprint(admin_blueprint, url_prefix='/admin')

    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint)

    from .home import home as home_blueprint
    app.register_blueprint(home_blueprint)

    return app

# request l'ho messo io #
from flask import abort, flash, redirect, render_template, url_for, request
from flask_login import current_user, login_required
from flask_wtf import FlaskForm
from . import admin
from .forms import DepartmentForm, EmployeeAssignForm, RoleForm, WordForm
from .. import db
from ..models import Department, Employee, Role, Word
from .. import dictionariesUploadSet
###
# Roba mia
from wtforms import FileField
#
###
# Roba mia
class MyForm(FlaskForm):
    dictionary = FileField('dictionary')
#
def check_admin():
    # prevent non-admins from accessing the page
    if not current_user.is_admin:
        abort(403)


# Department Views


@admin.route('/departments', methods=['GET', 'POST'])
@login_required
def list_departments():
    """
    List all departments
    """
    ###
    # Roba mia
    form = MyForm()
    #
    check_admin()

    departments = Department.query.all()
    ###
    # Roba mia

    if form.validate_on_submit():
        filename = dictionariesUploadSet.save(form.dictionary.data)
        return f'Filename: { filename }'
    #

    # form=form dentro al return è stato messo da me
    return render_template('admin/departments/departments.html',
                           departments=departments, title="Departments", form=form)

@admin.route('/words', methods=['GET', 'POST'])
@login_required
def list_words():
    """
    List all words
    """
    check_admin()

    words = Word.query.all()

    return render_template('admin/words/words.html',
                           words=words, title="Words")


@admin.route('/departments/add', methods=['GET', 'POST'])
@login_required
def add_department():
    """
    Add a department to the database
    """
    check_admin()

    add_department = True

    form = DepartmentForm()
    if form.validate_on_submit():
        department = Department(name=form.name.data,
                                description=form.description.data)
        try:
            # add department to the database
            db.session.add(department)
            db.session.commit()
            flash('You have successfully added a new department.')
        except:
            # in case department name already exists
            flash('Error: department name already exists.')

        # redirect to departments page
        return redirect(url_for('admin.list_departments'))

    # load department template
    return render_template('admin/departments/department.html', action="Add",
                           add_department=add_department, form=form,
                           title="Add Department")


@admin.route('/words/add', methods=['GET', 'POST'])
@login_required
def add_word():
    """
    Add a department to the database
    """
    check_admin()
    file = open("testo.txt", "r")
    file_write = open("testo_write.txt", "w")
    add_word = True

    form = WordForm()
    if form.validate_on_submit():
        for line in file:
            if line.__contains__("a\'"):
                # deve essere line=line.replace, mettere solo line.replace non funziona
                line = line.replace("a\'", "à")
            if line.__contains__("e\'"):
                # deve essere line=line.replace, mettere solo line.replace non funziona
                line = line.replace("e\'", "è")
            if line.__contains__("i\'"):
                # deve essere line=line.replace, mettere solo line.replace non funziona
                line = line.replace("i\'", "ì")
            if line.__contains__("o\'"):
                # deve essere line=line.replace, mettere solo line.replace non funziona
                line = line.replace("o\'", "ò")
            if line.__contains__("u\'"):
                # deve essere line=line.replace, mettere solo line.replace non funziona
                line = line.replace("u\'", "ù")
            file_write.write(line)
            word = Word(name=line, description=line)
            db.session.add(word)
            db.session.commit()
        word = Word(name=form.name.data, description=form.description.data)
        try:
            # add word to the database
            db.session.add(word)
            db.session.commit()
            flash('You have successfully added a new word.')
        except:
            # in case department name already exists
            flash('Error: word name already exists.')

        # redirect to words page
        return redirect(url_for('admin.list_words'))



    # load department template
    return render_template('admin/words/word.html', action="Add",
                           add_word=add_word, form=form,
                           title="Add Word")








@admin.route('/departments/edit/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_department(id):
    """
    Edit a department
    """
    check_admin()

    add_department = False

    department = Department.query.get_or_404(id)
    form = DepartmentForm(obj=department)
    if form.validate_on_submit():
        department.name = form.name.data
        department.description = form.description.data
        db.session.commit()
        flash('You have successfully edited the department.')

        # redirect to the departments page
        return redirect(url_for('admin.list_departments'))

    form.description.data = department.description
    form.name.data = department.name
    return render_template('admin/departments/department.html', action="Edit",
                           add_department=add_department, form=form,
                           department=department, title="Edit Department")

@admin.route('/words/edit/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_word(id):
    """
    Edit a word
    """
    check_admin()

    add_word = False

    word = Word.query.get_or_404(id)
    form = WordForm(obj=word)
    if form.validate_on_submit():
        word.name = form.name.data
        word.description = form.description.data
        db.session.commit()
        flash('You have successfully edited the word.')

        # redirect to the words page
        return redirect(url_for('admin.list_words'))

    form.description.data = word.description
    form.name.data = word.name
    return render_template('admin/words/word.html', action="Edit",
                           add_word=add_word, form=form,
                           word=word, title="Edit Word")

@admin.route('/departments/delete/<int:id>', methods=['GET', 'POST'])
@login_required
def delete_department(id):
    """
    Delete a department from the database
    """
    check_admin()

    department = Department.query.get_or_404(id)
    db.session.delete(department)
    db.session.commit()
    flash('You have successfully deleted the department.')

    # redirect to the departments page
    return redirect(url_for('admin.list_departments'))

    return render_template(title="Delete Department")

@admin.route('/words/delete/<int:id>', methods=['GET', 'POST'])
@login_required
def delete_word(id):
    """
    Delete a word from the database
    """
    check_admin()

    word = Word.query.get_or_404(id)
    db.session.delete(word)
    db.session.commit()
    flash('You have successfully deleted the word.')

    # redirect to the words page
    return redirect(url_for('admin.list_words'))

    return render_template(title="Delete Word")

# Role Views


@admin.route('/roles')
@login_required
def list_roles():
    check_admin()
    """
    List all roles
    """
    roles = Role.query.all()
    return render_template('admin/roles/roles.html',
                           roles=roles, title='Roles')


@admin.route('/roles/add', methods=['GET', 'POST'])
@login_required
def add_role():
    """
    Add a role to the database
    """
    check_admin()

    add_role = True

    form = RoleForm()
    if form.validate_on_submit():
        role = Role(name=form.name.data,
                    description=form.description.data)

        try:
            # add role to the database
            db.session.add(role)
            db.session.commit()
            flash('You have successfully added a new role.')
        except:
            # in case role name already exists
            flash('Error: role name already exists.')

        # redirect to the roles page
        return redirect(url_for('admin.list_roles'))

    # load role template
    return render_template('admin/roles/role.html', add_role=add_role,
                           form=form, title='Add Role')


@admin.route('/roles/edit/<int:id>', methods=['GET', 'POST'])
@login_required
def edit_role(id):
    """
    Edit a role
    """
    check_admin()

    add_role = False

    role = Role.query.get_or_404(id)
    form = RoleForm(obj=role)
    if form.validate_on_submit():
        role.name = form.name.data
        role.description = form.description.data
        db.session.add(role)
        db.session.commit()
        flash('You have successfully edited the role.')

        # redirect to the roles page
        return redirect(url_for('admin.list_roles'))

    form.description.data = role.description
    form.name.data = role.name
    return render_template('admin/roles/role.html', add_role=add_role,
                           form=form, title="Edit Role")


@admin.route('/roles/delete/<int:id>', methods=['GET', 'POST'])
@login_required
def delete_role(id):
    """
    Delete a role from the database
    """
    check_admin()

    role = Role.query.get_or_404(id)
    db.session.delete(role)
    db.session.commit()
    flash('You have successfully deleted the role.')

    # redirect to the roles page
    return redirect(url_for('admin.list_roles'))

    return render_template(title="Delete Role")


# Employee Views

@admin.route('/employees')
@login_required
def list_employees():
    """
    List all employees
    """
    check_admin()

    employees = Employee.query.all()
    return render_template('admin/employees/employees.html',
                           employees=employees, title='Employees')


@admin.route('/employees/assign/<int:id>', methods=['GET', 'POST'])
@login_required
def assign_employee(id):
    """
    Assign a department and a role to an employee
    """
    check_admin()

    employee = Employee.query.get_or_404(id)

    # prevent admin from being assigned a department or role
    if employee.is_admin:
        abort(403)

    form = EmployeeAssignForm(obj=employee)
    if form.validate_on_submit():
        employee.department = form.department.data
        employee.role = form.role.data
        db.session.add(employee)
        db.session.commit()
        flash('You have successfully assigned a department and role.')

        # redirect to the roles page
        return redirect(url_for('admin.list_employees'))

    return render_template('admin/employees/employee.html',
                           employee=employee, form=form,
                           title='Assign Employee')
